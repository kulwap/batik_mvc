/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mvc.DBUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 *
 * @author setsuga
 */
public class Koneksi {
    private static Connection con;
    private static Properties prop = new Properties();
    public static Connection getKoneksi() throws SQLException {
        if(con == null || con.isClosed()) {
            try {
                prop.load(new FileInputStream("/home/setsuga/JAVA/MVC/src/mvc/DBUtils/DBconfig.properties"));
                con = DriverManager.getConnection(prop.getProperty("jdbc.url"),prop.getProperty("jdbc.username"),prop.getProperty("jdbc.password"));
            } catch (IOException ex) {
                System.err.println("Gagal Membaca Konfigurasi"+ex.getMessage());
            } catch (SQLException ex) {
                System.err.println("Gagal Menghubungkan Database"+ex.getMessage());
            }
            //testing nilai koneksi null
            System.err.println("Koneksi Nulll....");
        }
        System.err.println("Berhasil menghubungkan database");
        return con;
    }
    
    
    // Testing koneksi
//    public static void main(String[] args) {
//        getKoneksi();
//    }
}
